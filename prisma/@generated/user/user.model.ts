import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Message } from '../message/message.model';
import { Role } from '../prisma/role.enum';
import { Room } from '../room/room.model';

@ObjectType()
export class User {

    @Field(() => ID, {
            nullable: false,
        })
    id!: string;

    @Field(() => String, {
            nullable: false,
        })
    email!: string;

    @Field(() => String, {
            nullable: false,
        })
    password!: string;

    @Field(() => String, {
            nullable: true,
        })
    name?: string;

    @Field(() => Role, {
            nullable: false,
            defaultValue: "USER",
        })
    role!: Role;

    @Field(() => Date, {
            nullable: false,
        })
    createdAt!: Date | string;

    @Field(() => Date, {
            nullable: false,
        })
    updatedAt!: Date | string;

    @Field(() => String, {
            nullable: true,
        })
    currentHashedRefreshToken?: string;

    @Field(() => Room, {
            nullable: true,
        })
    room?: Room;

    @Field(() => String, {
            nullable: true,
        })
    readonly roomId?: string;

    @Field(() => [Message], {
            nullable: false,
        })
    messages!: Array<Message>;
}
