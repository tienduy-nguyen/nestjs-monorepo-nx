import { ArgsType, Field, Int } from '@nestjs/graphql';
import { RoomOrderByInput } from './room-order-by.input';
import { RoomScalarFieldEnum } from './room-scalar-field.enum';
import { RoomWhereUniqueInput } from './room-where-unique.input';
import { RoomWhereInput } from './room-where.input';

@ArgsType()
export class FindFirstRoomArgs {
  @Field(() => RoomWhereInput, {
    nullable: true,
  })
  where?: RoomWhereInput;

  @Field(() => [RoomOrderByInput], {
    nullable: true,
  })
  orderBy?: Array<RoomOrderByInput>;

  @Field(() => RoomWhereUniqueInput, {
    nullable: true,
  })
  cursor?: RoomWhereUniqueInput;

  @Field(() => Int, {
    nullable: true,
  })
  take?: number;

  @Field(() => Int, {
    nullable: true,
  })
  skip?: number;

  @Field(() => [RoomScalarFieldEnum], {
    nullable: true,
  })
  distinct?: Array<RoomScalarFieldEnum>;
}
