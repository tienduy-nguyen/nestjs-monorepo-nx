import { Role } from '@nestjs-monorepo-nx/prisma/@generated/prisma/role.enum';
import { IsEmail, IsOptional, IsString } from 'class-validator';

export class UpdateUserDto {
  @IsEmail()
  @IsOptional()
  email?: string;

  @IsString()
  @IsOptional()
  name?: string;

  @IsOptional()
  role?: Role;

  @IsOptional()
  currentHashedRefreshToken?: string;
}
