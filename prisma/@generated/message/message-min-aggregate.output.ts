import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class MessageMinAggregate {
  @Field(() => String, {
    nullable: true,
  })
  id?: string;

  @Field(() => String, {
    nullable: true,
  })
  message?: string;

  @Field(() => Date, {
    nullable: true,
  })
  createdAt?: Date | string;

  @Field(() => Date, {
    nullable: true,
  })
  updatedAt?: Date | string;

  @Field(() => String, {
    nullable: true,
  })
  userId?: string;

  @Field(() => String, {
    nullable: true,
  })
  roomId?: string;
}
