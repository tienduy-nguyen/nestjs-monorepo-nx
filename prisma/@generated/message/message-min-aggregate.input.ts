import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class MessageMinAggregateInput {
  @Field(() => Boolean, {
    nullable: true,
  })
  id?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  message?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  createdAt?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  updatedAt?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  userId?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  roomId?: true;
}
