import { Field, InputType } from '@nestjs/graphql';
import { MessageUpdateWithoutUserInput } from './message-update-without-user.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';

@InputType()
export class MessageUpdateWithWhereUniqueWithoutUserInput {
  @Field(() => MessageWhereUniqueInput, {
    nullable: false,
  })
  where!: MessageWhereUniqueInput;

  @Field(() => MessageUpdateWithoutUserInput, {
    nullable: false,
  })
  data!: MessageUpdateWithoutUserInput;
}
