import { Field, InputType } from '@nestjs/graphql';
import { UserCreateOrConnectWithoutroomInput } from './user-create-or-connect-withoutroom.input';
import { UserCreateWithoutRoomInput } from './user-create-without-room.input';
import { UserScalarWhereInput } from './user-scalar-where.input';
import { UserUpdateManyWithWhereWithoutRoomInput } from './user-update-many-with-where-without-room.input';
import { UserUpdateWithWhereUniqueWithoutRoomInput } from './user-update-with-where-unique-without-room.input';
import { UserUpsertWithWhereUniqueWithoutRoomInput } from './user-upsert-with-where-unique-without-room.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserUpdateManyWithoutRoomInput {
  @Field(() => [UserCreateWithoutRoomInput], {
    nullable: true,
  })
  create?: Array<UserCreateWithoutRoomInput>;

  @Field(() => [UserCreateOrConnectWithoutroomInput], {
    nullable: true,
  })
  connectOrCreate?: Array<UserCreateOrConnectWithoutroomInput>;

  @Field(() => [UserUpsertWithWhereUniqueWithoutRoomInput], {
    nullable: true,
  })
  upsert?: Array<UserUpsertWithWhereUniqueWithoutRoomInput>;

  @Field(() => [UserWhereUniqueInput], {
    nullable: true,
  })
  connect?: Array<UserWhereUniqueInput>;

  @Field(() => [UserWhereUniqueInput], {
    nullable: true,
  })
  set?: Array<UserWhereUniqueInput>;

  @Field(() => [UserWhereUniqueInput], {
    nullable: true,
  })
  disconnect?: Array<UserWhereUniqueInput>;

  @Field(() => [UserWhereUniqueInput], {
    nullable: true,
  })
  delete?: Array<UserWhereUniqueInput>;

  @Field(() => [UserUpdateWithWhereUniqueWithoutRoomInput], {
    nullable: true,
  })
  update?: Array<UserUpdateWithWhereUniqueWithoutRoomInput>;

  @Field(() => [UserUpdateManyWithWhereWithoutRoomInput], {
    nullable: true,
  })
  updateMany?: Array<UserUpdateManyWithWhereWithoutRoomInput>;

  @Field(() => [UserScalarWhereInput], {
    nullable: true,
  })
  deleteMany?: Array<UserScalarWhereInput>;
}
