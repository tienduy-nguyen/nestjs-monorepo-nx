import { Message } from '@nestjs-monorepo-nx/prisma/@generated/message/message.model';
import {
  FindManyRoomArgs,
  RoomWhereUniqueInput,
} from '@nestjs-monorepo-nx/prisma/@generated/room';
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { CreateRoomDto, UpdateRoomDto } from './dto';

@Injectable()
export class RoomService {
  constructor(private prisma: PrismaService) {}

  public async getRooms(args?: FindManyRoomArgs) {
    return await this.prisma.room.findMany({ ...args });
  }
  public async getRoomByUniqueInput(where: RoomWhereUniqueInput) {
    return await this.prisma.room.findUnique({ where });
  }
  public async createRoom(data: CreateRoomDto) {
    return await this.prisma.room.create({
      data,
    });
  }

  public async updateRoom(where: RoomWhereUniqueInput, data: UpdateRoomDto) {
    return await this.prisma.room.update({ where, data });
  }

  public async deleteRoom(where: RoomWhereUniqueInput) {
    return await this.prisma.room.delete({ where });
  }

  public async sendMessage(message: string, userId: string, roomId?: string) {
    await this.prisma.message.create({
      data: {
        message,
        userId,
        roomId,
      },
    });
  }

  public async messagesOfRoom(roomId: string) {
    const room = await this.prisma.room.findUnique({
      where: { id: roomId },
      include: { messages: true },
    });
    return room.messages;
  }
}
