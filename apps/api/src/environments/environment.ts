import dotenv from 'dotenv';
import { IEnvironment } from './environment.interface';

export const environment = () => {
  if (process.env.NODE_ENV === 'production') {
    dotenv.config({ path: `${process.cwd()}/.env.production` });
  } else {
    dotenv.config();
  }

  return {
    siteUrl: process.env.SITE_URL,
    serverPort: Number(process.env.SERVER_PORT),
    // JWT
    jwtOptions: {
      privateKey: process.env.JWT_PRIVATE_KEY,
      publicKey: process.env.JWT_PUBLIC_KEY,
      secret: process.env.JWT_PRIVATE_KEY,
      secretOrPrivateKey: process.env.JWT_PRIVATE_KEY,
      signOptions: {
        expiresIn: Number(process.env.JWT_EXPIRE_TIME),
      },
      accessTokenExpiresIn: 60 * 60 * 2, //2h
      refreshTokenExpiresIn: 60 * 60 * 24 * 30, // 30 days
    },
    session: {
      secret: process.env.SESSION_SECRET,
    },
  } as IEnvironment;
};
