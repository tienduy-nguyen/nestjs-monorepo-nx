import { Field, InputType } from '@nestjs/graphql';
import { MessageUpdateManyWithoutUserInput } from '../message/message-update-many-without-user.input';
import { Role } from '../prisma/role.enum';

@InputType()
export class UserUncheckedUpdateWithoutRoomInput {

    @Field(() => String, {
            nullable: true,
        })
    id?: string;

    @Field(() => String, {
            nullable: true,
        })
    email?: string;

    @Field(() => String, {
            nullable: true,
        })
    password?: string;

    @Field(() => String, {
            nullable: true,
        })
    name?: string;

    @Field(() => Role, {
            nullable: true,
        })
    role?: Role;

    @Field(() => Date, {
            nullable: true,
        })
    createdAt?: Date | string;

    @Field(() => Date, {
            nullable: true,
        })
    updatedAt?: Date | string;

    @Field(() => String, {
            nullable: true,
        })
    currentHashedRefreshToken?: string;

    @Field(() => MessageUpdateManyWithoutUserInput, {
            nullable: true,
        })
    messages?: MessageUpdateManyWithoutUserInput;
}
