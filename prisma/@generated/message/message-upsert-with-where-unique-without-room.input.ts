import { Field, InputType } from '@nestjs/graphql';
import { MessageCreateWithoutRoomInput } from './message-create-without-room.input';
import { MessageUpdateWithoutRoomInput } from './message-update-without-room.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';

@InputType()
export class MessageUpsertWithWhereUniqueWithoutRoomInput {
  @Field(() => MessageWhereUniqueInput, {
    nullable: false,
  })
  where!: MessageWhereUniqueInput;

  @Field(() => MessageUpdateWithoutRoomInput, {
    nullable: false,
  })
  update!: MessageUpdateWithoutRoomInput;

  @Field(() => MessageCreateWithoutRoomInput, {
    nullable: false,
  })
  create!: MessageCreateWithoutRoomInput;
}
