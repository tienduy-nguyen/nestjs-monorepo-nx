import { Field, InputType } from '@nestjs/graphql';
import { MessageCreateWithoutUserInput } from './message-create-without-user.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';

@InputType()
export class MessageCreateOrConnectWithoutuserInput {
  @Field(() => MessageWhereUniqueInput, {
    nullable: false,
  })
  where!: MessageWhereUniqueInput;

  @Field(() => MessageCreateWithoutUserInput, {
    nullable: false,
  })
  create!: MessageCreateWithoutUserInput;
}
