import { IsNotEmpty, IsOptional, IsString, MinLength } from 'class-validator';

export class CreateRoomDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsOptional()
  @MinLength(10)
  description?: string;

  @IsOptional()
  isUser?: boolean;

  @IsOptional()
  isPrivate?: boolean;
}
