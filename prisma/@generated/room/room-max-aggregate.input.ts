import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class RoomMaxAggregateInput {
  @Field(() => Boolean, {
    nullable: true,
  })
  id?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  name?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  description?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  isUser?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  isPrivate?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  createdAt?: true;

  @Field(() => Boolean, {
    nullable: true,
  })
  updatedAt?: true;
}
