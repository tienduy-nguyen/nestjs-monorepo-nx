import { Field, InputType } from '@nestjs/graphql';
import { MessageCreateOrConnectWithoutroomInput } from './message-create-or-connect-withoutroom.input';
import { MessageCreateWithoutRoomInput } from './message-create-without-room.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';

@InputType()
export class MessageCreateManyWithoutRoomInput {
  @Field(() => [MessageCreateWithoutRoomInput], {
    nullable: true,
  })
  create?: Array<MessageCreateWithoutRoomInput>;

  @Field(() => [MessageCreateOrConnectWithoutroomInput], {
    nullable: true,
  })
  connectOrCreate?: Array<MessageCreateOrConnectWithoutroomInput>;

  @Field(() => [MessageWhereUniqueInput], {
    nullable: true,
  })
  connect?: Array<MessageWhereUniqueInput>;
}
