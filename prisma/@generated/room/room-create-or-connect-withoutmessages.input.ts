import { Field, InputType } from '@nestjs/graphql';
import { RoomCreateWithoutMessagesInput } from './room-create-without-messages.input';
import { RoomWhereUniqueInput } from './room-where-unique.input';

@InputType()
export class RoomCreateOrConnectWithoutmessagesInput {
  @Field(() => RoomWhereUniqueInput, {
    nullable: false,
  })
  where!: RoomWhereUniqueInput;

  @Field(() => RoomCreateWithoutMessagesInput, {
    nullable: false,
  })
  create!: RoomCreateWithoutMessagesInput;
}
