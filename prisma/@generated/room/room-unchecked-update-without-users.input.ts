import { Field, InputType } from '@nestjs/graphql';
import { MessageUpdateManyWithoutRoomInput } from '../message/message-update-many-without-room.input';

@InputType()
export class RoomUncheckedUpdateWithoutUsersInput {
  @Field(() => String, {
    nullable: true,
  })
  id?: string;

  @Field(() => String, {
    nullable: true,
  })
  name?: string;

  @Field(() => String, {
    nullable: true,
  })
  description?: string;

  @Field(() => Boolean, {
    nullable: true,
  })
  isUser?: boolean;

  @Field(() => Boolean, {
    nullable: true,
  })
  isPrivate?: boolean;

  @Field(() => Date, {
    nullable: true,
  })
  createdAt?: Date | string;

  @Field(() => Date, {
    nullable: true,
  })
  updatedAt?: Date | string;

  @Field(() => MessageUpdateManyWithoutRoomInput, {
    nullable: true,
  })
  messages?: MessageUpdateManyWithoutRoomInput;
}
