import { Injectable } from '@nestjs/common';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { UserService } from '../../user/user.service';
import { IPayloadUserJwt } from '@nestjs-monorepo-nx/api/src/common/interfaces/http.interface';
import { UserWhereUniqueInput } from '@nestjs-monorepo-nx/prisma/@generated/user';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        (req: Request) => {
          return req?.cookies?.Authorization;
        },
      ]),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }
  public async validate(payload: IPayloadUserJwt) {
    const where: UserWhereUniqueInput = {
      email: payload.email,
    };
    const user = await this.userService.getUserByUniqueInput(where);
    return user;
  }
}
