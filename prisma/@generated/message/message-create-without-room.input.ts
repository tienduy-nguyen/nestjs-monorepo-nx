import { Field, InputType } from '@nestjs/graphql';
import { UserCreateOneWithoutMessagesInput } from '../user/user-create-one-without-messages.input';

@InputType()
export class MessageCreateWithoutRoomInput {
  @Field(() => String, {
    nullable: true,
  })
  id?: string;

  @Field(() => String, {
    nullable: false,
  })
  message!: string;

  @Field(() => Date, {
    nullable: true,
  })
  createdAt?: Date | string;

  @Field(() => Date, {
    nullable: true,
  })
  updatedAt?: Date | string;

  @Field(() => UserCreateOneWithoutMessagesInput, {
    nullable: false,
  })
  user!: UserCreateOneWithoutMessagesInput;
}
