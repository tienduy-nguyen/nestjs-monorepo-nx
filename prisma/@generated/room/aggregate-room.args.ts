import { ArgsType, Field, Int } from '@nestjs/graphql';
import { RoomMaxAggregateInput } from './room-max-aggregate.input';
import { RoomMinAggregateInput } from './room-min-aggregate.input';
import { RoomOrderByInput } from './room-order-by.input';
import { RoomWhereUniqueInput } from './room-where-unique.input';
import { RoomWhereInput } from './room-where.input';

@ArgsType()
export class AggregateRoomArgs {
  @Field(() => RoomWhereInput, {
    nullable: true,
  })
  where?: RoomWhereInput;

  @Field(() => [RoomOrderByInput], {
    nullable: true,
  })
  orderBy?: Array<RoomOrderByInput>;

  @Field(() => RoomWhereUniqueInput, {
    nullable: true,
  })
  cursor?: RoomWhereUniqueInput;

  @Field(() => Int, {
    nullable: true,
  })
  take?: number;

  @Field(() => Int, {
    nullable: true,
  })
  skip?: number;

  @Field(() => Boolean, {
    nullable: true,
  })
  count?: true;

  @Field(() => RoomMinAggregateInput, {
    nullable: true,
  })
  min?: RoomMinAggregateInput;

  @Field(() => RoomMaxAggregateInput, {
    nullable: true,
  })
  max?: RoomMaxAggregateInput;
}
