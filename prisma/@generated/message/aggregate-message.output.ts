import { Field, ObjectType } from '@nestjs/graphql';
import { MessageCountAggregate } from './message-count-aggregate.output';
import { MessageMaxAggregate } from './message-max-aggregate.output';
import { MessageMinAggregate } from './message-min-aggregate.output';

@ObjectType()
export class AggregateMessage {
  @Field(() => MessageCountAggregate, {
    nullable: true,
  })
  count?: MessageCountAggregate;

  @Field(() => MessageMinAggregate, {
    nullable: true,
  })
  min?: MessageMinAggregate;

  @Field(() => MessageMaxAggregate, {
    nullable: true,
  })
  max?: MessageMaxAggregate;
}
