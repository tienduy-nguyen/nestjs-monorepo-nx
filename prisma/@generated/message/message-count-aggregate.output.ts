import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class MessageCountAggregate {
  @Field(() => Int, {
    nullable: true,
  })
  id?: number;

  @Field(() => Int, {
    nullable: true,
  })
  message?: number;

  @Field(() => Int, {
    nullable: true,
  })
  createdAt?: number;

  @Field(() => Int, {
    nullable: true,
  })
  updatedAt?: number;

  @Field(() => Int, {
    nullable: true,
  })
  userId?: number;

  @Field(() => Int, {
    nullable: true,
  })
  roomId?: number;

  @Field(() => Int, {
    nullable: false,
  })
  _all!: number;
}
