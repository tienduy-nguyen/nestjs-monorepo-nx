import { Field, InputType } from '@nestjs/graphql';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { EnumRoleFilter } from '../prisma/enum-role-filter.input';
import { StringFilter } from '../prisma/string-filter.input';

@InputType()
export class UserScalarWhereInput {

    @Field(() => [UserScalarWhereInput], {
            nullable: true,
        })
    AND?: Array<UserScalarWhereInput>;

    @Field(() => [UserScalarWhereInput], {
            nullable: true,
        })
    OR?: Array<UserScalarWhereInput>;

    @Field(() => [UserScalarWhereInput], {
            nullable: true,
        })
    NOT?: Array<UserScalarWhereInput>;

    @Field(() => StringFilter, {
            nullable: true,
        })
    id?: StringFilter;

    @Field(() => StringFilter, {
            nullable: true,
        })
    email?: StringFilter;

    @Field(() => StringFilter, {
            nullable: true,
        })
    password?: StringFilter;

    @Field(() => StringFilter, {
            nullable: true,
        })
    name?: StringFilter;

    @Field(() => EnumRoleFilter, {
            nullable: true,
        })
    role?: EnumRoleFilter;

    @Field(() => DateTimeFilter, {
            nullable: true,
        })
    createdAt?: DateTimeFilter;

    @Field(() => DateTimeFilter, {
            nullable: true,
        })
    updatedAt?: DateTimeFilter;

    @Field(() => StringFilter, {
            nullable: true,
        })
    currentHashedRefreshToken?: StringFilter;

    @Field(() => StringFilter, {
            nullable: true,
        })
    roomId?: StringFilter;
}
