import { Field, InputType } from '@nestjs/graphql';
import { MessageCreateWithoutRoomInput } from './message-create-without-room.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';

@InputType()
export class MessageCreateOrConnectWithoutroomInput {
  @Field(() => MessageWhereUniqueInput, {
    nullable: false,
  })
  where!: MessageWhereUniqueInput;

  @Field(() => MessageCreateWithoutRoomInput, {
    nullable: false,
  })
  create!: MessageCreateWithoutRoomInput;
}
