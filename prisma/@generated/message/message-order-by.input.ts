import { Field, InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';
import { RoomOrderByInput } from '../room/room-order-by.input';
import { UserOrderByInput } from '../user/user-order-by.input';

@InputType()
export class MessageOrderByInput {
  @Field(() => SortOrder, {
    nullable: true,
  })
  id?: SortOrder;

  @Field(() => SortOrder, {
    nullable: true,
  })
  message?: SortOrder;

  @Field(() => SortOrder, {
    nullable: true,
  })
  createdAt?: SortOrder;

  @Field(() => SortOrder, {
    nullable: true,
  })
  updatedAt?: SortOrder;

  @Field(() => UserOrderByInput, {
    nullable: true,
  })
  user?: UserOrderByInput;

  @Field(() => SortOrder, {
    nullable: true,
  })
  userId?: SortOrder;

  @Field(() => RoomOrderByInput, {
    nullable: true,
  })
  room?: RoomOrderByInput;

  @Field(() => SortOrder, {
    nullable: true,
  })
  roomId?: SortOrder;
}
