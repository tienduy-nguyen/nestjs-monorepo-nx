import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { NestExpressApplication } from '@nestjs/platform-express';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import hpp from 'hpp';
import RateLimit from 'express-rate-limit';
import compression from 'compression';
import { setupSwagger } from './common/configs/setup-swagger';
import { AppModule } from './app.module';
import passport from 'passport';
import session from 'express-session';
import Redis from 'ioredis';
import { ISessionOption } from './environments/environment.interface';
import { sessionConfig } from './common/configs/session.config';
import { environment } from './environments/environment';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const env = environment();
  const port: number = env.serverPort;
  const siteUrl: string = env.siteUrl;
  const config = app.get(ConfigService);

  app.use(helmet());
  app.use(cors());
  app.use(hpp());
  app.use(helmet());
  app.use(compression());
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(cookieParser());

  if (process.env.NODE_ENV === 'production') {
    app.enable('trust proxy'); // only if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
    app.use(compression());
    app.use(helmet());

    app.use(
      RateLimit({
        windowMs: 15 * 60 * 1000, // 15 minutes
        max: 100, // limit each IP to 100 requests per windowMs
      }),
    );
  }

  // Setup session with redis
  const redisClient = new Redis();
  const sessionEnv: ISessionOption = env.session;
  const sessionOptions = sessionConfig(redisClient, sessionEnv);
  app.use(session(sessionOptions));

  // init 'passport' (npm install passport)
  app.use(passport.initialize());
  app.use(passport.session());

  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);

  setupSwagger(app);
  await app.listen(port, () => {
    Logger.log(`Listening at ${siteUrl}/${globalPrefix}`);
    Logger.log(`Running in ${config.get('NODE_ENV')} mode`);
  });
}

bootstrap();
