import { Field, InputType } from '@nestjs/graphql';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { StringFilter } from '../prisma/string-filter.input';
import { RoomWhereInput } from '../room/room-where.input';
import { UserWhereInput } from '../user/user-where.input';

@InputType()
export class MessageWhereInput {
  @Field(() => [MessageWhereInput], {
    nullable: true,
  })
  AND?: Array<MessageWhereInput>;

  @Field(() => [MessageWhereInput], {
    nullable: true,
  })
  OR?: Array<MessageWhereInput>;

  @Field(() => [MessageWhereInput], {
    nullable: true,
  })
  NOT?: Array<MessageWhereInput>;

  @Field(() => StringFilter, {
    nullable: true,
  })
  id?: StringFilter;

  @Field(() => StringFilter, {
    nullable: true,
  })
  message?: StringFilter;

  @Field(() => DateTimeFilter, {
    nullable: true,
  })
  createdAt?: DateTimeFilter;

  @Field(() => DateTimeFilter, {
    nullable: true,
  })
  updatedAt?: DateTimeFilter;

  @Field(() => UserWhereInput, {
    nullable: true,
  })
  user?: UserWhereInput;

  @Field(() => StringFilter, {
    nullable: true,
  })
  userId?: StringFilter;

  @Field(() => RoomWhereInput, {
    nullable: true,
  })
  room?: RoomWhereInput;

  @Field(() => StringFilter, {
    nullable: true,
  })
  roomId?: StringFilter;
}
