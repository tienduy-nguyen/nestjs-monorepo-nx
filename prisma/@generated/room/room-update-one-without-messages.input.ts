import { Field, InputType } from '@nestjs/graphql';
import { RoomCreateOrConnectWithoutmessagesInput } from './room-create-or-connect-withoutmessages.input';
import { RoomCreateWithoutMessagesInput } from './room-create-without-messages.input';
import { RoomUpdateWithoutMessagesInput } from './room-update-without-messages.input';
import { RoomUpsertWithoutMessagesInput } from './room-upsert-without-messages.input';
import { RoomWhereUniqueInput } from './room-where-unique.input';

@InputType()
export class RoomUpdateOneWithoutMessagesInput {
  @Field(() => RoomCreateWithoutMessagesInput, {
    nullable: true,
  })
  create?: RoomCreateWithoutMessagesInput;

  @Field(() => RoomCreateOrConnectWithoutmessagesInput, {
    nullable: true,
  })
  connectOrCreate?: RoomCreateOrConnectWithoutmessagesInput;

  @Field(() => RoomUpsertWithoutMessagesInput, {
    nullable: true,
  })
  upsert?: RoomUpsertWithoutMessagesInput;

  @Field(() => RoomWhereUniqueInput, {
    nullable: true,
  })
  connect?: RoomWhereUniqueInput;

  @Field(() => Boolean, {
    nullable: true,
  })
  disconnect?: boolean;

  @Field(() => Boolean, {
    nullable: true,
  })
  delete?: boolean;

  @Field(() => RoomUpdateWithoutMessagesInput, {
    nullable: true,
  })
  update?: RoomUpdateWithoutMessagesInput;
}
