import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class MessageUncheckedCreateInput {
  @Field(() => String, {
    nullable: true,
  })
  id?: string;

  @Field(() => String, {
    nullable: false,
  })
  message!: string;

  @Field(() => Date, {
    nullable: true,
  })
  createdAt?: Date | string;

  @Field(() => Date, {
    nullable: true,
  })
  updatedAt?: Date | string;

  @Field(() => String, {
    nullable: false,
  })
  userId!: string;

  @Field(() => String, {
    nullable: true,
  })
  roomId?: string;
}
