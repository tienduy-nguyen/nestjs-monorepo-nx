import { Role } from '@nestjs-monorepo-nx/prisma/@generated/prisma/role.enum';
import { IsEmail, IsOptional, IsString, MinLength } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsString()
  @MinLength(3)
  password: string;

  @IsString()
  @IsOptional()
  name?: string;

  @IsOptional()
  role?: Role;
}
