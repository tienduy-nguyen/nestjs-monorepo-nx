import { registerEnumType } from '@nestjs/graphql';

export enum RoomScalarFieldEnum {
  id = 'id',
  name = 'name',
  description = 'description',
  isUser = 'isUser',
  isPrivate = 'isPrivate',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}

registerEnumType(RoomScalarFieldEnum, { name: 'RoomScalarFieldEnum' });
