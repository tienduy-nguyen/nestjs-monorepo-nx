import { Field, InputType } from '@nestjs/graphql';
import { RoomUpdateOneWithoutMessagesInput } from '../room/room-update-one-without-messages.input';

@InputType()
export class MessageUpdateWithoutUserInput {
  @Field(() => String, {
    nullable: true,
  })
  id?: string;

  @Field(() => String, {
    nullable: true,
  })
  message?: string;

  @Field(() => Date, {
    nullable: true,
  })
  createdAt?: Date | string;

  @Field(() => Date, {
    nullable: true,
  })
  updatedAt?: Date | string;

  @Field(() => RoomUpdateOneWithoutMessagesInput, {
    nullable: true,
  })
  room?: RoomUpdateOneWithoutMessagesInput;
}
