import { Field, InputType } from '@nestjs/graphql';
import { MessageCreateWithoutUserInput } from './message-create-without-user.input';
import { MessageUpdateWithoutUserInput } from './message-update-without-user.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';

@InputType()
export class MessageUpsertWithWhereUniqueWithoutUserInput {
  @Field(() => MessageWhereUniqueInput, {
    nullable: false,
  })
  where!: MessageWhereUniqueInput;

  @Field(() => MessageUpdateWithoutUserInput, {
    nullable: false,
  })
  update!: MessageUpdateWithoutUserInput;

  @Field(() => MessageCreateWithoutUserInput, {
    nullable: false,
  })
  create!: MessageCreateWithoutUserInput;
}
