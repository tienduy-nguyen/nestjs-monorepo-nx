import { Field, InputType } from '@nestjs/graphql';
import { RoomCreateWithoutUsersInput } from './room-create-without-users.input';
import { RoomUpdateWithoutUsersInput } from './room-update-without-users.input';

@InputType()
export class RoomUpsertWithoutUsersInput {
  @Field(() => RoomUpdateWithoutUsersInput, {
    nullable: false,
  })
  update!: RoomUpdateWithoutUsersInput;

  @Field(() => RoomCreateWithoutUsersInput, {
    nullable: false,
  })
  create!: RoomCreateWithoutUsersInput;
}
