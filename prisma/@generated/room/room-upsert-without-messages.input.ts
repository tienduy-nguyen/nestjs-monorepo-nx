import { Field, InputType } from '@nestjs/graphql';
import { RoomCreateWithoutMessagesInput } from './room-create-without-messages.input';
import { RoomUpdateWithoutMessagesInput } from './room-update-without-messages.input';

@InputType()
export class RoomUpsertWithoutMessagesInput {
  @Field(() => RoomUpdateWithoutMessagesInput, {
    nullable: false,
  })
  update!: RoomUpdateWithoutMessagesInput;

  @Field(() => RoomCreateWithoutMessagesInput, {
    nullable: false,
  })
  create!: RoomCreateWithoutMessagesInput;
}
