import { Field, InputType } from '@nestjs/graphql';
import { MessageUpdateWithoutRoomInput } from './message-update-without-room.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';

@InputType()
export class MessageUpdateWithWhereUniqueWithoutRoomInput {
  @Field(() => MessageWhereUniqueInput, {
    nullable: false,
  })
  where!: MessageWhereUniqueInput;

  @Field(() => MessageUpdateWithoutRoomInput, {
    nullable: false,
  })
  data!: MessageUpdateWithoutRoomInput;
}
