import { Field, InputType } from '@nestjs/graphql';
import { UserUpdateOneRequiredWithoutMessagesInput } from '../user/user-update-one-required-without-messages.input';

@InputType()
export class MessageUpdateWithoutRoomInput {
  @Field(() => String, {
    nullable: true,
  })
  id?: string;

  @Field(() => String, {
    nullable: true,
  })
  message?: string;

  @Field(() => Date, {
    nullable: true,
  })
  createdAt?: Date | string;

  @Field(() => Date, {
    nullable: true,
  })
  updatedAt?: Date | string;

  @Field(() => UserUpdateOneRequiredWithoutMessagesInput, {
    nullable: true,
  })
  user?: UserUpdateOneRequiredWithoutMessagesInput;
}
