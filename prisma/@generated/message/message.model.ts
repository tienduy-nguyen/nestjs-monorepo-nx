import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Room } from '../room/room.model';
import { User } from '../user/user.model';

@ObjectType()
export class Message {
  @Field(() => ID, {
    nullable: false,
  })
  id!: string;

  @Field(() => String, {
    nullable: false,
  })
  message!: string;

  @Field(() => Date, {
    nullable: false,
  })
  createdAt!: Date | string;

  @Field(() => Date, {
    nullable: false,
  })
  updatedAt!: Date | string;

  @Field(() => User, {
    nullable: false,
  })
  user!: User;

  @Field(() => String, {
    nullable: false,
  })
  readonly userId!: string;

  @Field(() => Room, {
    nullable: true,
  })
  room?: Room;

  @Field(() => String, {
    nullable: true,
  })
  readonly roomId?: string;
}
