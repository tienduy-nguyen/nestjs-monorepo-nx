import { Field, InputType } from '@nestjs/graphql';
import { MessageCreateOrConnectWithoutuserInput } from './message-create-or-connect-withoutuser.input';
import { MessageCreateWithoutUserInput } from './message-create-without-user.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';

@InputType()
export class MessageCreateManyWithoutUserInput {
  @Field(() => [MessageCreateWithoutUserInput], {
    nullable: true,
  })
  create?: Array<MessageCreateWithoutUserInput>;

  @Field(() => [MessageCreateOrConnectWithoutuserInput], {
    nullable: true,
  })
  connectOrCreate?: Array<MessageCreateOrConnectWithoutuserInput>;

  @Field(() => [MessageWhereUniqueInput], {
    nullable: true,
  })
  connect?: Array<MessageWhereUniqueInput>;
}
