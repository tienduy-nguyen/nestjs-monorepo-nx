import { Field, InputType } from '@nestjs/graphql';
import { UserCreateOrConnectWithoutroomInput } from './user-create-or-connect-withoutroom.input';
import { UserCreateWithoutRoomInput } from './user-create-without-room.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateManyWithoutRoomInput {
  @Field(() => [UserCreateWithoutRoomInput], {
    nullable: true,
  })
  create?: Array<UserCreateWithoutRoomInput>;

  @Field(() => [UserCreateOrConnectWithoutroomInput], {
    nullable: true,
  })
  connectOrCreate?: Array<UserCreateOrConnectWithoutroomInput>;

  @Field(() => [UserWhereUniqueInput], {
    nullable: true,
  })
  connect?: Array<UserWhereUniqueInput>;
}
