import { Field, ObjectType } from '@nestjs/graphql';
import { RoomCountAggregate } from './room-count-aggregate.output';
import { RoomMaxAggregate } from './room-max-aggregate.output';
import { RoomMinAggregate } from './room-min-aggregate.output';

@ObjectType()
export class AggregateRoom {
  @Field(() => RoomCountAggregate, {
    nullable: true,
  })
  count?: RoomCountAggregate;

  @Field(() => RoomMinAggregate, {
    nullable: true,
  })
  min?: RoomMinAggregate;

  @Field(() => RoomMaxAggregate, {
    nullable: true,
  })
  max?: RoomMaxAggregate;
}
