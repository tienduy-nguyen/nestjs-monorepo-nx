import { UserWhereUniqueInput } from '@nestjs-monorepo-nx/prisma/@generated/user';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateUserDto, UpdateUserDto } from './dto';
import { UserService } from './user.service';
import { ApiTags } from '@nestjs/swagger';

@Controller('users')
@ApiTags('Users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  public async getUsers() {
    const users = await this.userService.getUsers();
    return users;
  }

  @Get('/:id')
  public async getUser(@Param('id') id: string) {
    const where: UserWhereUniqueInput = {
      id,
    };
    return await this.userService.getUserByUniqueInput(where);
  }

  @Post()
  public async create(@Body() data: CreateUserDto) {
    return await this.userService.createUser(data);
  }

  @Put('/:id')
  public async updateUser(
    @Param('id') id: string,
    @Body() data: UpdateUserDto,
  ) {
    const where: UserWhereUniqueInput = {
      id,
    };
    return await this.userService.updateUser(where, data);
  }

  @Delete('/:id')
  public async deleteUser(@Param('id') id: string) {
    const where: UserWhereUniqueInput = {
      id,
    };
    return await this.userService.deleteUser(where);
  }
}
