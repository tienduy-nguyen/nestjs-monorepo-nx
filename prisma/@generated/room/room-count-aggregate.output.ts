import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class RoomCountAggregate {
  @Field(() => Int, {
    nullable: true,
  })
  id?: number;

  @Field(() => Int, {
    nullable: true,
  })
  name?: number;

  @Field(() => Int, {
    nullable: true,
  })
  description?: number;

  @Field(() => Int, {
    nullable: true,
  })
  isUser?: number;

  @Field(() => Int, {
    nullable: true,
  })
  isPrivate?: number;

  @Field(() => Int, {
    nullable: true,
  })
  createdAt?: number;

  @Field(() => Int, {
    nullable: true,
  })
  updatedAt?: number;

  @Field(() => Int, {
    nullable: false,
  })
  _all!: number;
}
