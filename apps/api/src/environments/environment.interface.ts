import { JwtModuleOptions } from '@nestjs/jwt';

export interface IEnvironment {
  readonly siteUrl?: string;
  readonly serverPort: number;
  readonly jwtOptions?: IJwtOptions;
  readonly session?: ISessionOption;
}

export interface ISessionOption {
  secret: string;
}

export interface IJwtOptions extends JwtModuleOptions {
  accessTokenExpiresIn: number;
  refreshTokenExpiresIn: number;
}
