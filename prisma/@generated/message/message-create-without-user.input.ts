import { Field, InputType } from '@nestjs/graphql';
import { RoomCreateOneWithoutMessagesInput } from '../room/room-create-one-without-messages.input';

@InputType()
export class MessageCreateWithoutUserInput {
  @Field(() => String, {
    nullable: true,
  })
  id?: string;

  @Field(() => String, {
    nullable: false,
  })
  message!: string;

  @Field(() => Date, {
    nullable: true,
  })
  createdAt?: Date | string;

  @Field(() => Date, {
    nullable: true,
  })
  updatedAt?: Date | string;

  @Field(() => RoomCreateOneWithoutMessagesInput, {
    nullable: true,
  })
  room?: RoomCreateOneWithoutMessagesInput;
}
