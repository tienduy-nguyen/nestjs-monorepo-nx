import {} from '@nestjs/graphql';
import { GqlModuleOptions } from '@nestjs/graphql';
import { join } from 'path';

export const configuration = () => ({
  environment: process.env.NODE_ENV,
  port: parseInt(process.env.PORT || '3000', 10),
  graphql: {
    playground: true,
    autoSchemaFile: join(process.cwd(), 'libs/core/src/lib/graphql/schema.gql'),
  } as GqlModuleOptions,
});
