import { Module } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { PasswordService } from './passport.service';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [],
  controllers: [UserController],
  providers: [UserService, PrismaService, PasswordService],
  exports: [UserService, PasswordService],
})
export class UserModule {}
