import { Field, InputType } from '@nestjs/graphql';
import { RoomCreateOrConnectWithoutmessagesInput } from './room-create-or-connect-withoutmessages.input';
import { RoomCreateWithoutMessagesInput } from './room-create-without-messages.input';
import { RoomWhereUniqueInput } from './room-where-unique.input';

@InputType()
export class RoomCreateOneWithoutMessagesInput {
  @Field(() => RoomCreateWithoutMessagesInput, {
    nullable: true,
  })
  create?: RoomCreateWithoutMessagesInput;

  @Field(() => RoomCreateOrConnectWithoutmessagesInput, {
    nullable: true,
  })
  connectOrCreate?: RoomCreateOrConnectWithoutmessagesInput;

  @Field(() => RoomWhereUniqueInput, {
    nullable: true,
  })
  connect?: RoomWhereUniqueInput;
}
