import { UserWhereUniqueInput } from '@nestjs-monorepo-nx/prisma/@generated/user';
import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import {
  IPayloadUserJwt,
  ISessionAuthToken,
} from '@nestjs-monorepo-nx/api/src/common/interfaces/http.interface';
import { PasswordService } from '../user/passport.service';
import { UserService } from '../user/user.service';
import { RegisterUserDto } from './dto';
import { environment } from '../../environments/environment';
@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private passwordService: PasswordService,
  ) {}

  public async validateUser(email: string, password: string) {
    const where: UserWhereUniqueInput = {
      email: email,
    };
    const user = await this.userService.getUserByUniqueInput(where);
    if (!user) {
      throw new BadRequestException('Invalid credentials');
    }
    const isMatchedPassword = await this.passwordService.validatePassword(
      password,
      user.password,
    );
    if (!isMatchedPassword) {
      throw new BadRequestException('Invalid credentials');
    }
    return user;
  }

  public async register(data: RegisterUserDto) {
    const user = await this.userService.createUser(data);
    return user;
  }

  public async generateAuthTokenFromLogin(payload: IPayloadUserJwt) {
    const envJwt = environment().jwtOptions;
    const accessTokenExpiresIn = envJwt.accessTokenExpiresIn;
    const refreshTokenExpiresIn = envJwt.accessTokenExpiresIn;

    const sessionAuthToken: ISessionAuthToken = {
      accessToken: await this.jwtService.signAsync(payload, {
        expiresIn: accessTokenExpiresIn,
      }),
      refreshToken: await this.jwtService.signAsync(payload, {
        expiresIn: refreshTokenExpiresIn,
      }),
    };
    return sessionAuthToken;
  }

  public async resetCurrentHashesRefreshToken(
    where: UserWhereUniqueInput,
    refreshToken: string,
  ) {
    const currentHashedRefreshToken = await this.passwordService.hashPassword(
      refreshToken,
    );

    const user = await this.userService.updateUser(where, {
      currentHashedRefreshToken,
    });
    return user;
  }

  public async resetAccessToken(payload: IPayloadUserJwt) {
    const envJwt = environment().jwtOptions;
    const accessTokenExpiresIn = envJwt.accessTokenExpiresIn;

    const accessToken = await this.jwtService.signAsync(payload, {
      expiresIn: accessTokenExpiresIn,
    });
    return accessToken;
  }
}
