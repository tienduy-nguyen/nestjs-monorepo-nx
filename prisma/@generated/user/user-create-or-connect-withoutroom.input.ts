import { Field, InputType } from '@nestjs/graphql';
import { UserCreateWithoutRoomInput } from './user-create-without-room.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateOrConnectWithoutroomInput {
  @Field(() => UserWhereUniqueInput, {
    nullable: false,
  })
  where!: UserWhereUniqueInput;

  @Field(() => UserCreateWithoutRoomInput, {
    nullable: false,
  })
  create!: UserCreateWithoutRoomInput;
}
