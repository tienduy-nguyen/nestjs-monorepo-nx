import { ArgsType, Field, Int } from '@nestjs/graphql';
import { MessageMaxAggregateInput } from './message-max-aggregate.input';
import { MessageMinAggregateInput } from './message-min-aggregate.input';
import { MessageOrderByInput } from './message-order-by.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';
import { MessageWhereInput } from './message-where.input';

@ArgsType()
export class AggregateMessageArgs {
  @Field(() => MessageWhereInput, {
    nullable: true,
  })
  where?: MessageWhereInput;

  @Field(() => [MessageOrderByInput], {
    nullable: true,
  })
  orderBy?: Array<MessageOrderByInput>;

  @Field(() => MessageWhereUniqueInput, {
    nullable: true,
  })
  cursor?: MessageWhereUniqueInput;

  @Field(() => Int, {
    nullable: true,
  })
  take?: number;

  @Field(() => Int, {
    nullable: true,
  })
  skip?: number;

  @Field(() => Boolean, {
    nullable: true,
  })
  count?: true;

  @Field(() => MessageMinAggregateInput, {
    nullable: true,
  })
  min?: MessageMinAggregateInput;

  @Field(() => MessageMaxAggregateInput, {
    nullable: true,
  })
  max?: MessageMaxAggregateInput;
}
