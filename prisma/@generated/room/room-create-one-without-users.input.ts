import { Field, InputType } from '@nestjs/graphql';
import { RoomCreateOrConnectWithoutusersInput } from './room-create-or-connect-withoutusers.input';
import { RoomCreateWithoutUsersInput } from './room-create-without-users.input';
import { RoomWhereUniqueInput } from './room-where-unique.input';

@InputType()
export class RoomCreateOneWithoutUsersInput {
  @Field(() => RoomCreateWithoutUsersInput, {
    nullable: true,
  })
  create?: RoomCreateWithoutUsersInput;

  @Field(() => RoomCreateOrConnectWithoutusersInput, {
    nullable: true,
  })
  connectOrCreate?: RoomCreateOrConnectWithoutusersInput;

  @Field(() => RoomWhereUniqueInput, {
    nullable: true,
  })
  connect?: RoomWhereUniqueInput;
}
