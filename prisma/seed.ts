import { PrismaClient } from '@prisma/client';
import * as dotenv from 'dotenv';
import * as bcrypt from 'bcrypt';
import * as faker from 'faker';

const prisma = new PrismaClient();

async function main() {
  dotenv.config();
  console.log('Seeding ....');

  await prisma.$connect();

  /*********************************************************************/
  /* Dangerous zone: */
  /* Delete all tables before seeding */
  // Only uncomment these block code below if you want to delete for all existed tables

  await prisma.message.deleteMany({});
  await prisma.user.deleteMany({});
  await prisma.room.deleteMany({});

  //-----------------------------------------------
  /* Room seeding */
  const room1 = await prisma.room.upsert({
    where: { name: 'TypeScript' },
    create: {
      name: 'TypesScript',
      description: 'All things about TypeScript',
    },
    update: {},
  });

  const room2 = await prisma.room.upsert({
    where: { name: 'NestJS' },
    create: {
      name: 'NestJS',
      description: 'All things about NestJS',
    },
    update: {},
  });

  //-----------------------------------------------
  /* User seeding */
  const user1 = await prisma.user.upsert({
    where: { email: 'admin1@yopmail.com' },
    create: {
      email: 'admin1@yopmail.com',
      password: await bcrypt.hash('1234567', 12),
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      role: 'ADMIN',
      roomId: room1.id,
    },
    update: {},
  });

  const user2 = await prisma.user.upsert({
    where: { email: 'user1@yopmail.com' },
    create: {
      email: 'user1@yopmail.com',
      password: await bcrypt.hash('1234567', 12),
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      role: 'USER',
      roomId: room1.id,
    },
    update: {},
  });

  const user3 = await prisma.user.upsert({
    where: { email: 'user3@yopmail.com' },
    create: {
      email: 'user3@yopmail.com',
      password: await bcrypt.hash('1234567', 12),
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      role: 'USER',
      roomId: room2.id,
    },
    update: {},
  });

  const user4 = await prisma.user.upsert({
    where: { email: 'user4@yopmail.com' },
    create: {
      email: 'user4@yopmail.com',
      password: await bcrypt.hash('1234567', 12),
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      role: 'USER',
      roomId: room2.id,
    },
    update: {},
  });

  /* Message seeding */
  const arrayRoomId = [room1.id, room2.id];
  const arrayUserId = [user1.id, user2.id, user3.id, user4.id];

  const getRandom = (array: string[]) => {
    return array[Math.floor(Math.random() * array.length)];
  };

  for (let step = 0; step < 20; ++step) {
    await prisma.message.create({
      data: {
        message: faker.hacker.phrase(),
        userId: getRandom(arrayUserId),
        roomId: getRandom(arrayRoomId),
      },
    });
  }
}

main()
  .catch((e) => console.error(e))
  .finally(async () => {
    await prisma.$disconnect();
    console.log('Finish .....................................');
  });
