import { Field, InputType } from '@nestjs/graphql';
import { MessageCreateOrConnectWithoutroomInput } from './message-create-or-connect-withoutroom.input';
import { MessageCreateWithoutRoomInput } from './message-create-without-room.input';
import { MessageScalarWhereInput } from './message-scalar-where.input';
import { MessageUpdateManyWithWhereWithoutRoomInput } from './message-update-many-with-where-without-room.input';
import { MessageUpdateWithWhereUniqueWithoutRoomInput } from './message-update-with-where-unique-without-room.input';
import { MessageUpsertWithWhereUniqueWithoutRoomInput } from './message-upsert-with-where-unique-without-room.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';

@InputType()
export class MessageUpdateManyWithoutRoomInput {
  @Field(() => [MessageCreateWithoutRoomInput], {
    nullable: true,
  })
  create?: Array<MessageCreateWithoutRoomInput>;

  @Field(() => [MessageCreateOrConnectWithoutroomInput], {
    nullable: true,
  })
  connectOrCreate?: Array<MessageCreateOrConnectWithoutroomInput>;

  @Field(() => [MessageUpsertWithWhereUniqueWithoutRoomInput], {
    nullable: true,
  })
  upsert?: Array<MessageUpsertWithWhereUniqueWithoutRoomInput>;

  @Field(() => [MessageWhereUniqueInput], {
    nullable: true,
  })
  connect?: Array<MessageWhereUniqueInput>;

  @Field(() => [MessageWhereUniqueInput], {
    nullable: true,
  })
  set?: Array<MessageWhereUniqueInput>;

  @Field(() => [MessageWhereUniqueInput], {
    nullable: true,
  })
  disconnect?: Array<MessageWhereUniqueInput>;

  @Field(() => [MessageWhereUniqueInput], {
    nullable: true,
  })
  delete?: Array<MessageWhereUniqueInput>;

  @Field(() => [MessageUpdateWithWhereUniqueWithoutRoomInput], {
    nullable: true,
  })
  update?: Array<MessageUpdateWithWhereUniqueWithoutRoomInput>;

  @Field(() => [MessageUpdateManyWithWhereWithoutRoomInput], {
    nullable: true,
  })
  updateMany?: Array<MessageUpdateManyWithWhereWithoutRoomInput>;

  @Field(() => [MessageScalarWhereInput], {
    nullable: true,
  })
  deleteMany?: Array<MessageScalarWhereInput>;
}
