import { Field, InputType } from '@nestjs/graphql';
import { UserCreateWithoutRoomInput } from './user-create-without-room.input';
import { UserUpdateWithoutRoomInput } from './user-update-without-room.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserUpsertWithWhereUniqueWithoutRoomInput {
  @Field(() => UserWhereUniqueInput, {
    nullable: false,
  })
  where!: UserWhereUniqueInput;

  @Field(() => UserUpdateWithoutRoomInput, {
    nullable: false,
  })
  update!: UserUpdateWithoutRoomInput;

  @Field(() => UserCreateWithoutRoomInput, {
    nullable: false,
  })
  create!: UserCreateWithoutRoomInput;
}
