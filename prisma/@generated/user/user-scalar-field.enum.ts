import { registerEnumType } from '@nestjs/graphql';

export enum UserScalarFieldEnum {
  id = 'id',
  email = 'email',
  password = 'password',
  name = 'name',
  role = 'role',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
  roomId = 'roomId',
}

registerEnumType(UserScalarFieldEnum, { name: 'UserScalarFieldEnum' });
