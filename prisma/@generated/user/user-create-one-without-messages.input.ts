import { Field, InputType } from '@nestjs/graphql';
import { UserCreateOrConnectWithoutmessagesInput } from './user-create-or-connect-withoutmessages.input';
import { UserCreateWithoutMessagesInput } from './user-create-without-messages.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserCreateOneWithoutMessagesInput {
  @Field(() => UserCreateWithoutMessagesInput, {
    nullable: true,
  })
  create?: UserCreateWithoutMessagesInput;

  @Field(() => UserCreateOrConnectWithoutmessagesInput, {
    nullable: true,
  })
  connectOrCreate?: UserCreateOrConnectWithoutmessagesInput;

  @Field(() => UserWhereUniqueInput, {
    nullable: true,
  })
  connect?: UserWhereUniqueInput;
}
