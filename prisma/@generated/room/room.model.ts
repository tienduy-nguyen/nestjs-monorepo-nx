import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Message } from '../message/message.model';
import { User } from '../user/user.model';

@ObjectType()
export class Room {
  @Field(() => ID, {
    nullable: false,
  })
  id!: string;

  @Field(() => String, {
    nullable: false,
  })
  name!: string;

  @Field(() => String, {
    nullable: true,
  })
  description?: string;

  @Field(() => Boolean, {
    nullable: false,
    defaultValue: false,
  })
  isUser!: boolean;

  @Field(() => Boolean, {
    nullable: false,
    defaultValue: false,
  })
  isPrivate!: boolean;

  @Field(() => Date, {
    nullable: false,
  })
  createdAt!: Date | string;

  @Field(() => Date, {
    nullable: false,
  })
  updatedAt!: Date | string;

  @Field(() => [User], {
    nullable: false,
  })
  users!: Array<User>;

  @Field(() => [Message], {
    nullable: false,
  })
  messages!: Array<Message>;
}
