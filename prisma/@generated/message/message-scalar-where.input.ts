import { Field, InputType } from '@nestjs/graphql';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { StringFilter } from '../prisma/string-filter.input';

@InputType()
export class MessageScalarWhereInput {
  @Field(() => [MessageScalarWhereInput], {
    nullable: true,
  })
  AND?: Array<MessageScalarWhereInput>;

  @Field(() => [MessageScalarWhereInput], {
    nullable: true,
  })
  OR?: Array<MessageScalarWhereInput>;

  @Field(() => [MessageScalarWhereInput], {
    nullable: true,
  })
  NOT?: Array<MessageScalarWhereInput>;

  @Field(() => StringFilter, {
    nullable: true,
  })
  id?: StringFilter;

  @Field(() => StringFilter, {
    nullable: true,
  })
  message?: StringFilter;

  @Field(() => DateTimeFilter, {
    nullable: true,
  })
  createdAt?: DateTimeFilter;

  @Field(() => DateTimeFilter, {
    nullable: true,
  })
  updatedAt?: DateTimeFilter;

  @Field(() => StringFilter, {
    nullable: true,
  })
  userId?: StringFilter;

  @Field(() => StringFilter, {
    nullable: true,
  })
  roomId?: StringFilter;
}
