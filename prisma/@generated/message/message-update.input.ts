import { Field, InputType } from '@nestjs/graphql';
import { RoomUpdateOneWithoutMessagesInput } from '../room/room-update-one-without-messages.input';
import { UserUpdateOneRequiredWithoutMessagesInput } from '../user/user-update-one-required-without-messages.input';

@InputType()
export class MessageUpdateInput {
  @Field(() => String, {
    nullable: true,
  })
  id?: string;

  @Field(() => String, {
    nullable: true,
  })
  message?: string;

  @Field(() => Date, {
    nullable: true,
  })
  createdAt?: Date | string;

  @Field(() => Date, {
    nullable: true,
  })
  updatedAt?: Date | string;

  @Field(() => UserUpdateOneRequiredWithoutMessagesInput, {
    nullable: true,
  })
  user?: UserUpdateOneRequiredWithoutMessagesInput;

  @Field(() => RoomUpdateOneWithoutMessagesInput, {
    nullable: true,
  })
  room?: RoomUpdateOneWithoutMessagesInput;
}
