import { Field, InputType } from '@nestjs/graphql';
import { RoomCreateOrConnectWithoutusersInput } from './room-create-or-connect-withoutusers.input';
import { RoomCreateWithoutUsersInput } from './room-create-without-users.input';
import { RoomUpdateWithoutUsersInput } from './room-update-without-users.input';
import { RoomUpsertWithoutUsersInput } from './room-upsert-without-users.input';
import { RoomWhereUniqueInput } from './room-where-unique.input';

@InputType()
export class RoomUpdateOneWithoutUsersInput {
  @Field(() => RoomCreateWithoutUsersInput, {
    nullable: true,
  })
  create?: RoomCreateWithoutUsersInput;

  @Field(() => RoomCreateOrConnectWithoutusersInput, {
    nullable: true,
  })
  connectOrCreate?: RoomCreateOrConnectWithoutusersInput;

  @Field(() => RoomUpsertWithoutUsersInput, {
    nullable: true,
  })
  upsert?: RoomUpsertWithoutUsersInput;

  @Field(() => RoomWhereUniqueInput, {
    nullable: true,
  })
  connect?: RoomWhereUniqueInput;

  @Field(() => Boolean, {
    nullable: true,
  })
  disconnect?: boolean;

  @Field(() => Boolean, {
    nullable: true,
  })
  delete?: boolean;

  @Field(() => RoomUpdateWithoutUsersInput, {
    nullable: true,
  })
  update?: RoomUpdateWithoutUsersInput;
}
