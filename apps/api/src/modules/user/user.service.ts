import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import {
  FindManyUserArgs,
  UserWhereUniqueInput,
} from '@nestjs-monorepo-nx/prisma/@generated/user';
import { CreateUserDto, UpdateUserDto } from './dto';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  public async getUsers(args?: FindManyUserArgs) {
    return await this.prisma.user.findMany({ ...args });
  }
  public async getUserByUniqueInput(where: UserWhereUniqueInput) {
    return await this.prisma.user.findUnique({ where });
  }
  public async createUser(data: CreateUserDto) {
    return await this.prisma.user.create({
      data,
    });
  }
  public async updateUser(where: UserWhereUniqueInput, data: UpdateUserDto) {
    return await this.prisma.user.update({ where, data });
  }

  public async deleteUser(where: UserWhereUniqueInput) {
    return await this.prisma.user.delete({ where });
  }
}
