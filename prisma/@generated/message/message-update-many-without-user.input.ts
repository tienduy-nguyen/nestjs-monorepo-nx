import { Field, InputType } from '@nestjs/graphql';
import { MessageCreateOrConnectWithoutuserInput } from './message-create-or-connect-withoutuser.input';
import { MessageCreateWithoutUserInput } from './message-create-without-user.input';
import { MessageScalarWhereInput } from './message-scalar-where.input';
import { MessageUpdateManyWithWhereWithoutUserInput } from './message-update-many-with-where-without-user.input';
import { MessageUpdateWithWhereUniqueWithoutUserInput } from './message-update-with-where-unique-without-user.input';
import { MessageUpsertWithWhereUniqueWithoutUserInput } from './message-upsert-with-where-unique-without-user.input';
import { MessageWhereUniqueInput } from './message-where-unique.input';

@InputType()
export class MessageUpdateManyWithoutUserInput {
  @Field(() => [MessageCreateWithoutUserInput], {
    nullable: true,
  })
  create?: Array<MessageCreateWithoutUserInput>;

  @Field(() => [MessageCreateOrConnectWithoutuserInput], {
    nullable: true,
  })
  connectOrCreate?: Array<MessageCreateOrConnectWithoutuserInput>;

  @Field(() => [MessageUpsertWithWhereUniqueWithoutUserInput], {
    nullable: true,
  })
  upsert?: Array<MessageUpsertWithWhereUniqueWithoutUserInput>;

  @Field(() => [MessageWhereUniqueInput], {
    nullable: true,
  })
  connect?: Array<MessageWhereUniqueInput>;

  @Field(() => [MessageWhereUniqueInput], {
    nullable: true,
  })
  set?: Array<MessageWhereUniqueInput>;

  @Field(() => [MessageWhereUniqueInput], {
    nullable: true,
  })
  disconnect?: Array<MessageWhereUniqueInput>;

  @Field(() => [MessageWhereUniqueInput], {
    nullable: true,
  })
  delete?: Array<MessageWhereUniqueInput>;

  @Field(() => [MessageUpdateWithWhereUniqueWithoutUserInput], {
    nullable: true,
  })
  update?: Array<MessageUpdateWithWhereUniqueWithoutUserInput>;

  @Field(() => [MessageUpdateManyWithWhereWithoutUserInput], {
    nullable: true,
  })
  updateMany?: Array<MessageUpdateManyWithWhereWithoutUserInput>;

  @Field(() => [MessageScalarWhereInput], {
    nullable: true,
  })
  deleteMany?: Array<MessageScalarWhereInput>;
}
