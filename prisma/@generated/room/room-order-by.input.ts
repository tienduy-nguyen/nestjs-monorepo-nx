import { Field, InputType } from '@nestjs/graphql';
import { SortOrder } from '../prisma/sort-order.enum';

@InputType()
export class RoomOrderByInput {
  @Field(() => SortOrder, {
    nullable: true,
  })
  id?: SortOrder;

  @Field(() => SortOrder, {
    nullable: true,
  })
  name?: SortOrder;

  @Field(() => SortOrder, {
    nullable: true,
  })
  description?: SortOrder;

  @Field(() => SortOrder, {
    nullable: true,
  })
  isUser?: SortOrder;

  @Field(() => SortOrder, {
    nullable: true,
  })
  isPrivate?: SortOrder;

  @Field(() => SortOrder, {
    nullable: true,
  })
  createdAt?: SortOrder;

  @Field(() => SortOrder, {
    nullable: true,
  })
  updatedAt?: SortOrder;
}
