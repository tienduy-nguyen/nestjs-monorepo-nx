import { Field, InputType } from '@nestjs/graphql';
import { UserUpdateWithoutRoomInput } from './user-update-without-room.input';
import { UserWhereUniqueInput } from './user-where-unique.input';

@InputType()
export class UserUpdateWithWhereUniqueWithoutRoomInput {
  @Field(() => UserWhereUniqueInput, {
    nullable: false,
  })
  where!: UserWhereUniqueInput;

  @Field(() => UserUpdateWithoutRoomInput, {
    nullable: false,
  })
  data!: UserUpdateWithoutRoomInput;
}
