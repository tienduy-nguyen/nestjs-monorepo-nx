import { ArgsType, Field } from '@nestjs/graphql';
import { RoomWhereUniqueInput } from './room-where-unique.input';

@ArgsType()
export class FindOneRoomArgs {
  @Field(() => RoomWhereUniqueInput, {
    nullable: false,
  })
  where!: RoomWhereUniqueInput;
}
