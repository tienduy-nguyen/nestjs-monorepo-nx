import { Field, InputType } from '@nestjs/graphql';
import { Role } from '../prisma/role.enum';
import { RoomUpdateOneWithoutUsersInput } from '../room/room-update-one-without-users.input';

@InputType()
export class UserUpdateWithoutMessagesInput {

    @Field(() => String, {
            nullable: true,
        })
    id?: string;

    @Field(() => String, {
            nullable: true,
        })
    email?: string;

    @Field(() => String, {
            nullable: true,
        })
    password?: string;

    @Field(() => String, {
            nullable: true,
        })
    name?: string;

    @Field(() => Role, {
            nullable: true,
        })
    role?: Role;

    @Field(() => Date, {
            nullable: true,
        })
    createdAt?: Date | string;

    @Field(() => Date, {
            nullable: true,
        })
    updatedAt?: Date | string;

    @Field(() => String, {
            nullable: true,
        })
    currentHashedRefreshToken?: string;

    @Field(() => RoomUpdateOneWithoutUsersInput, {
            nullable: true,
        })
    room?: RoomUpdateOneWithoutUsersInput;
}
