import { Field, InputType } from '@nestjs/graphql';
import { MessageListRelationFilter } from '../message/message-list-relation-filter.input';
import { BoolFilter } from '../prisma/bool-filter.input';
import { DateTimeFilter } from '../prisma/date-time-filter.input';
import { StringFilter } from '../prisma/string-filter.input';
import { UserListRelationFilter } from '../user/user-list-relation-filter.input';

@InputType()
export class RoomWhereInput {
  @Field(() => [RoomWhereInput], {
    nullable: true,
  })
  AND?: Array<RoomWhereInput>;

  @Field(() => [RoomWhereInput], {
    nullable: true,
  })
  OR?: Array<RoomWhereInput>;

  @Field(() => [RoomWhereInput], {
    nullable: true,
  })
  NOT?: Array<RoomWhereInput>;

  @Field(() => StringFilter, {
    nullable: true,
  })
  id?: StringFilter;

  @Field(() => StringFilter, {
    nullable: true,
  })
  name?: StringFilter;

  @Field(() => StringFilter, {
    nullable: true,
  })
  description?: StringFilter;

  @Field(() => BoolFilter, {
    nullable: true,
  })
  isUser?: BoolFilter;

  @Field(() => BoolFilter, {
    nullable: true,
  })
  isPrivate?: BoolFilter;

  @Field(() => DateTimeFilter, {
    nullable: true,
  })
  createdAt?: DateTimeFilter;

  @Field(() => DateTimeFilter, {
    nullable: true,
  })
  updatedAt?: DateTimeFilter;

  @Field(() => UserListRelationFilter, {
    nullable: true,
  })
  users?: UserListRelationFilter;

  @Field(() => MessageListRelationFilter, {
    nullable: true,
  })
  messages?: MessageListRelationFilter;
}
