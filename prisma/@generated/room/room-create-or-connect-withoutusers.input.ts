import { Field, InputType } from '@nestjs/graphql';
import { RoomCreateWithoutUsersInput } from './room-create-without-users.input';
import { RoomWhereUniqueInput } from './room-where-unique.input';

@InputType()
export class RoomCreateOrConnectWithoutusersInput {
  @Field(() => RoomWhereUniqueInput, {
    nullable: false,
  })
  where!: RoomWhereUniqueInput;

  @Field(() => RoomCreateWithoutUsersInput, {
    nullable: false,
  })
  create!: RoomCreateWithoutUsersInput;
}
