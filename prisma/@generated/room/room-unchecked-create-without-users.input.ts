import { Field, InputType } from '@nestjs/graphql';
import { MessageCreateManyWithoutRoomInput } from '../message/message-create-many-without-room.input';

@InputType()
export class RoomUncheckedCreateWithoutUsersInput {
  @Field(() => String, {
    nullable: true,
  })
  id?: string;

  @Field(() => String, {
    nullable: false,
  })
  name!: string;

  @Field(() => String, {
    nullable: true,
  })
  description?: string;

  @Field(() => Boolean, {
    nullable: true,
  })
  isUser?: boolean;

  @Field(() => Boolean, {
    nullable: true,
  })
  isPrivate?: boolean;

  @Field(() => Date, {
    nullable: true,
  })
  createdAt?: Date | string;

  @Field(() => Date, {
    nullable: true,
  })
  updatedAt?: Date | string;

  @Field(() => MessageCreateManyWithoutRoomInput, {
    nullable: true,
  })
  messages?: MessageCreateManyWithoutRoomInput;
}
