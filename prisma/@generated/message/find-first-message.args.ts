import { ArgsType, Field, Int } from '@nestjs/graphql';
import { MessageOrderByInput } from './message-order-by.input';
import { MessageScalarFieldEnum } from './message-scalar-field.enum';
import { MessageWhereUniqueInput } from './message-where-unique.input';
import { MessageWhereInput } from './message-where.input';

@ArgsType()
export class FindFirstMessageArgs {
  @Field(() => MessageWhereInput, {
    nullable: true,
  })
  where?: MessageWhereInput;

  @Field(() => [MessageOrderByInput], {
    nullable: true,
  })
  orderBy?: Array<MessageOrderByInput>;

  @Field(() => MessageWhereUniqueInput, {
    nullable: true,
  })
  cursor?: MessageWhereUniqueInput;

  @Field(() => Int, {
    nullable: true,
  })
  take?: number;

  @Field(() => Int, {
    nullable: true,
  })
  skip?: number;

  @Field(() => [MessageScalarFieldEnum], {
    nullable: true,
  })
  distinct?: Array<MessageScalarFieldEnum>;
}
