import { Field, InputType } from '@nestjs/graphql';
import { MessageCreateManyWithoutUserInput } from '../message/message-create-many-without-user.input';
import { Role } from '../prisma/role.enum';

@InputType()
export class UserUncheckedCreateWithoutRoomInput {

    @Field(() => String, {
            nullable: true,
        })
    id?: string;

    @Field(() => String, {
            nullable: false,
        })
    email!: string;

    @Field(() => String, {
            nullable: false,
        })
    password!: string;

    @Field(() => String, {
            nullable: true,
        })
    name?: string;

    @Field(() => Role, {
            nullable: true,
        })
    role?: Role;

    @Field(() => Date, {
            nullable: true,
        })
    createdAt?: Date | string;

    @Field(() => Date, {
            nullable: true,
        })
    updatedAt?: Date | string;

    @Field(() => String, {
            nullable: true,
        })
    currentHashedRefreshToken?: string;

    @Field(() => MessageCreateManyWithoutUserInput, {
            nullable: true,
        })
    messages?: MessageCreateManyWithoutUserInput;
}
