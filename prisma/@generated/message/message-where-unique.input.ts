import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class MessageWhereUniqueInput {
  @Field(() => String, {
    nullable: true,
  })
  id?: string;
}
