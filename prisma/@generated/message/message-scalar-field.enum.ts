import { registerEnumType } from '@nestjs/graphql';

export enum MessageScalarFieldEnum {
  id = 'id',
  message = 'message',
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
  userId = 'userId',
  roomId = 'roomId',
}

registerEnumType(MessageScalarFieldEnum, { name: 'MessageScalarFieldEnum' });
