import { IsOptional, IsString, MinLength } from 'class-validator';

export class UpdateRoomDto {
  @IsString()
  @IsOptional()
  name?: string;

  @IsString()
  @IsOptional()
  @MinLength(10)
  description?: string;

  @IsOptional()
  isUser?: boolean;

  @IsOptional()
  isPrivate?: boolean;
}
